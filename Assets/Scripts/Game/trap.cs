﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trap : MonoBehaviour {
    public Material withglow;
	public Material normal;
	public bool isRunning = false;
    private void OnTriggerEnter(Collider other)
    {
		if (other.tag == "Player" && !isRunning) {
			StartCoroutine (TrapTimer (other));
        }
    }
	private void OnColliderStay(Collider other)
	{
		gameObject.GetComponent<MeshRenderer>().material = withglow;
	}
	private void OnTriggerExit (Collider other){
		
	if (other.tag == "Player") {
		gameObject.GetComponent<MeshRenderer> ().material = normal;
	}
}
	IEnumerator TrapTimer(Collider other)
	{
		float timer = 5f;
		isRunning = true;
		Rigidbody rigidbody = other.GetComponent<Rigidbody> ();
		gameObject.GetComponent<MeshRenderer>().material = withglow;
		rigidbody.isKinematic = true;
		while (timer > 0) 
		{
			timer -= Time.deltaTime;
			yield return null;
		}
		isRunning = false;
		rigidbody.isKinematic = false;
	}
}