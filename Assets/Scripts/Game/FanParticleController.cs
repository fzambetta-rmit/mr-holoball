﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanParticleController : MonoBehaviour
{
    public GameObject p;
    bool isPlaying = false;

    /*
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            PlayExplosion();
        }

        transform.Rotate(Vector3.up * 0.5f);
    }
    */

    public void PlayExplosion()
    {
        if(!isPlaying)
        {
            isPlaying = true;
            StartCoroutine(PlayDelay());
        }
    }

    IEnumerator PlayDelay()
    {
        float timer = 3f;
        p.SetActive(true);

        while(timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }

        p.SetActive(false);
        isPlaying = false;
    }

 
	

}
