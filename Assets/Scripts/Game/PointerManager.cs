﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RMIT.GameTech;

public class PointerManager : Singleton<PointerManager> {
    public GameObject PointerPrefab;
    private List<Pointer> pointers;

	// Use this for initialization
	void Start () {
        pointers = new List<Pointer>();

        // Delete ALL children.
        for (int i = 0; i < transform.childCount; ++i)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public Pointer SpawnPointer(Transform spiderTransform, GameObject prefab)
    {
        GameObject obj = GameObject.Instantiate(prefab, transform, false);
        Pointer pointer = obj.GetComponent<Pointer>();
        pointer.Target = spiderTransform;
        pointers.Add(pointer);
        return pointer;
    }

    public Pointer SpawnPointer(Transform spiderTransform)
    {
		return SpawnPointer(spiderTransform, PointerPrefab);
    }

	// Update is called once per frame
	/*void Update () {
		
	}*/
}
