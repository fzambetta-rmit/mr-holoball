﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bumpPad : MonoBehaviour {

    public Animator anim;
    private int dirX;
    private int dirZ;

    private void Start()
    {
        dirX = 5;
        dirZ = 3;
    }

    private void OnTriggerEnter(Collider other)
    {
		if (other.tag == "Player")
        {
            other.GetComponent<Rigidbody>().velocity = new Vector3(other.GetComponent<Rigidbody>().velocity.x * (-1) + dirX, 0, other.GetComponent<Rigidbody>().velocity.z * (-1) + dirZ);
            //Debug.Log("Collided with bumpPad");

            anim.SetTrigger("Trigger");
        }
    }
}
