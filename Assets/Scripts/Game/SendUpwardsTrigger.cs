﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendUpwardsTrigger : MonoBehaviour {
	public string StringToSendUpwards;

	void OnTriggerEnter(Collider other)
	{
		other.gameObject.SendMessageUpwards (StringToSendUpwards, SendMessageOptions.DontRequireReceiver);
	}
}
