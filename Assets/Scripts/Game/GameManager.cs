﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;
using System;
using RMIT.GameTech;
using SpatialQueries;
using HoloToolkit.Unity.InputModule;

public class GameManager : RMIT.GameTech.Singleton<GameManager>, IInputClickHandler {

    public enum eGameState
    {
        WaitingToScan,
        ScanningPlayspace,
		ScanningCompleted,
        ReadyToPlay,
        Playing,
        // -------
        COUNT
    }

    private enum eSurfaceType
    {
        None,
        Floor,
        Ceiling,
        Wall,
        COUNT
    }

    // Use this for initialization
    public GameObject EndPortalPrefab;
    public GameObject StartPortalPrefab;
    public TMPro.TextMeshPro SystemText;

    public BallManager ballManager;
    private StartPortal startPortal;
    private EndPortal endPortal;
    private SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult startPortalResult = null;
    private SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult lastStartPortalResult = null;
    private SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult endPortalResult;
    private eSurfaceType lastEndPortalType = eSurfaceType.Floor;
    private eGameState gameState;
	public FeatureSpawner featureSpawner;

    public float MinAreaForStats = 5.0f;
    public float MinAreaForComplete = 50.0f;
    public float MinHorizAreaForComplete = 25.0f;
    public float MinWallAreaForComplete = 10.0f;

	public LayerMask obstacleMask;

	private SpatialQuery spawns_floor;
	private SpatialQuery spawns_walls;
	private SpatialQuery spawns_ceiling;

    private Vector3 lastBallGravity;

    void Start () {
        InputManager.Instance.AddGlobalListener(gameObject);

        if (ballManager == null)
        {
            ballManager = BallManager.Instance;
        }
			
        EventManager.StartListening(EventManager.Events.StartGame, StartGame);
        EventManager.StartListening(EventManager.Events.NewGame, NewGame);
        EventManager.StartListening(EventManager.Events.EndGame, NewGame);
        EventManager.StartListening(EventManager.Events.StartScan, StartScanningRoom);
        EventManager.StartListening(EventManager.Events.StopScan, StopScanningRoom);
        //SpatialMapQueries.Instance.Query_Portals();

        if (DoesScanMeetMinBarForCompletion)
            gameState = eGameState.ReadyToPlay;
        else
            gameState = eGameState.WaitingToScan;
        StartScanningRoom();

        //SetSystemText("");
        //StartGame();
        lastBallGravity = Vector3.down;
        SpatialUnderstanding.Instance.OnScanDone += ScanDone;

    }

	const float queryhalfDim = 0.1f;

    [ContextMenu("Start Game")]
    public void StartGame()
    {
        if (gameState == eGameState.ReadyToPlay)
        {
            //int portalCount = portalQuery.PlacementResults.Count;
            //int startIndex = -1;
            Vector3 newPosition;
            Quaternion newRotation;
            SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult result;

            if (startPortal == null)
            {
                if (endPortalResult != null)
                    result = endPortalResult;
                else
                    result = GetAvailablePosition (spawns_floor); //spawns_floor.PlacementResults[startIndex].Result;

                lastStartPortalResult = startPortalResult;
                startPortalResult = result;
                newPosition = result.Position - (result.Up * queryhalfDim);
                newRotation = Quaternion.LookRotation(result.Forward, result.Up);

                GameObject obj = GameObject.Instantiate(StartPortalPrefab, newPosition, newRotation);
                startPortal = obj.GetComponent<StartPortal>();
                startPortal.startGravity = lastBallGravity;
                obj.SetActive(true);
            }

			if (endPortal == null)
			{
                lastEndPortalType = GetOtherType(lastEndPortalType);
                result = GetAvailablePosition(GetSpatialResultFromType(lastEndPortalType), lastStartPortalResult);

                switch (lastEndPortalType)
                {
                    case eSurfaceType.Floor:
                        newPosition = result.Position - (result.Up * queryhalfDim);
                        newRotation = Quaternion.LookRotation(result.Forward, result.Up);
                        break;
                    case eSurfaceType.Ceiling:
                        newPosition = result.Position - (-result.Up * queryhalfDim);
                        newRotation = Quaternion.LookRotation(result.Forward, -result.Up);
                        break;
                    case eSurfaceType.Wall:
                        newPosition = result.Position - (result.Forward * queryhalfDim);
                        newRotation = Quaternion.LookRotation(result.Up, result.Forward);
                        break;
                    default:
                        newPosition = result.Position - (result.Up * queryhalfDim);
                        newRotation = Quaternion.LookRotation(result.Forward, result.Up);
                        break;
                }

				GameObject obj = GameObject.Instantiate(EndPortalPrefab, newPosition, newRotation);
				endPortal = obj.GetComponent<EndPortal>();
                endPortalResult = result;
				
				startPortal.endPortal = endPortal;
				obj.SetActive(true);
			}

			//Interns
			/*SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult portal1Result = GetAvailablePosition (spawns_ceiling);
			Quaternion portal1Rotation = Quaternion.LookRotation (portal1Result.Forward, -portal1Result.Up);
			SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult portal2Result = GetAvailablePosition (spawns_floor);
			Quaternion portal2Rotation = Quaternion.LookRotation (portal2Result.Forward, portal2Result.Up);

			featureSpawner.SpawnPortals (portal1Result.Position, portal1Rotation, PortalType.Roof, portal2Result.Position, portal2Rotation, PortalType.Floor);*/

			//SpawnFeatureAtRandomPosition (0,1);
			//SpawnFeatureAtRandomPosition (1,1);
			//SpawnFeatureAtRandomPosition (2,4);
            //SpawnFeatureAtRandomPosition (3,1);
			//SpawnFeatureAtRandomPosition (4,1);





            Debug.Log("Start Game");
        }
    }
    public void OnInputClicked(InputClickedEventData eventData)
    {
        if (gameState == eGameState.ScanningPlayspace)
        {
            EventManager.TriggerEvent(EventManager.Events.StopScan);
        }
    }
    
    private eSurfaceType GetOtherType(eSurfaceType type)
    {
        bool left = UnityEngine.Random.Range(0f, 1f) < 0.5f;
        switch (type)
        {
            case eSurfaceType.Floor:
                return left ? eSurfaceType.Ceiling : eSurfaceType.Wall;
            case eSurfaceType.Ceiling:
                return left ? eSurfaceType.Floor : eSurfaceType.Wall;
            case eSurfaceType.Wall:
                return left ? eSurfaceType.Ceiling : eSurfaceType.Floor;
            default:
                break;
        }

        return eSurfaceType.Floor;
    }

    private SpatialQuery GetSpatialResultFromType(eSurfaceType type)
    {
        switch (type)
        {
            case eSurfaceType.Floor:
                return spawns_floor;
            case eSurfaceType.Ceiling:
                return spawns_ceiling;
            case eSurfaceType.Wall:
                return spawns_walls;
            default:
                break;
        }

        return spawns_floor;
    }

	[ContextMenu("End Game")]
	public void EndGame()
	{
        lastBallGravity = ballManager.GetFirstBall().gravityNormal;
        ballManager.ClearBalls ();

		if (startPortal != null)
		{
			Destroy (startPortal.gameObject);
			startPortal = null;
		}

		if (endPortal != null)
		{
			Destroy (endPortal.gameObject);
			endPortal = null;
		}
			
		Debug.Log("End Game");
	}

    public void NewGame()
    {
        EndGame();
        StartGame();
    }

    public void ClearGame()
    {
        if (startPortal != null)
            Destroy(startPortal.gameObject);

        ballManager.ClearBalls();
    }

	// Update is called once per frame
	void Update () {
        switch(gameState)
        {
			case eGameState.ScanningPlayspace:
				SetSystemText (GetScanningText ());
			break;
            default:
                break;
        }

		if (Input.GetKeyDown (KeyCode.O)) {
			EventManager.TriggerEvent (EventManager.Events.StartScan);
		}

		if (Input.GetKeyDown (KeyCode.P)) {
			EventManager.TriggerEvent (EventManager.Events.StopScan);
		}
	}

    public bool DoesScanMeetMinBarForCompletion
    {
        get
        {
            // Only allow this when we are actually scanning
            if ((SpatialUnderstanding.Instance.ScanState != SpatialUnderstanding.ScanStates.Scanning) ||
                (!SpatialUnderstanding.Instance.AllowSpatialUnderstanding))
            {
                return false;
            }

            // Query the current playspace stats
            IntPtr statsPtr = SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceStatsPtr();
            if (SpatialUnderstandingDll.Imports.QueryPlayspaceStats(statsPtr) == 0)
            {
                return false;
            }
            SpatialUnderstandingDll.Imports.PlayspaceStats stats = SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceStats();

            // Check our preset requirements
            if ((stats.TotalSurfaceArea > MinAreaForComplete) ||
                (stats.HorizSurfaceArea > MinHorizAreaForComplete) ||
                (stats.WallSurfaceArea > MinWallAreaForComplete))
            {
                return true;
            }
            return false;
        }
    }


    public string GetScanningText()
    {
        // Scan state
        if (SpatialUnderstanding.Instance.AllowSpatialUnderstanding)
        {
            switch (SpatialUnderstanding.Instance.ScanState)
            {
                case SpatialUnderstanding.ScanStates.Scanning:
                    // Get the scan stats
                    IntPtr statsPtr = SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceStatsPtr();
                    if (SpatialUnderstandingDll.Imports.QueryPlayspaceStats(statsPtr) == 0)
                    {
                        return "playspace stats query failed";
                    }

                    // The stats tell us if we could potentially finish
                    if (DoesScanMeetMinBarForCompletion)
                    {
                        return "Say \"stop scan\" or Tap to start playing";
                    }
                    return "Scan your playspace";
                case SpatialUnderstanding.ScanStates.Finishing:
                    return "LOADING...";
                case SpatialUnderstanding.ScanStates.Done:
                    return "Scan complete - Use the menu to run queries";
                default:
                    return "ScanState = " + SpatialUnderstanding.Instance.ScanState.ToString();
            }
        }
        return "";
    }

    public void StartScanningRoom()
    {
        if (SpatialUnderstanding.Instance.ScanState == SpatialUnderstanding.ScanStates.None ||
			SpatialUnderstanding.Instance.ScanState == SpatialUnderstanding.ScanStates.Done)
        {
            ClearGame();
            SetSystemText("Scanning");
            SpatialUnderstanding.Instance.RequestBeginScanning();
            gameState = eGameState.ScanningPlayspace;
        }
    }

    public void StopScanningRoom()
    {
        if ((SpatialUnderstanding.Instance.ScanState == SpatialUnderstanding.ScanStates.Scanning) &&
               !SpatialUnderstanding.Instance.ScanStatsReportStillWorking)
        {
            SpatialUnderstanding.Instance.RequestFinishScan();
        }
    }

    void ScanDone()
	{
		Debug.Log ("Scan Done");
		SetSystemText ("Please Wait");
		gameState = eGameState.ScanningCompleted;

		// Kick off Spatial Queries
		Invoke("StartQueries", 1.0f);
    }

	void SpawnPortalQueriesDone()
	{
		if (spawns_floor == null || spawns_ceiling == null || spawns_walls == null)
			return;

		if (spawns_floor.PlacementResults.Count == 0 ||
			(spawns_ceiling.PlacementResults.Count == 0 ||
			 spawns_walls.PlacementResults.Count == 0))
			return;
			
		
		gameState = eGameState.ReadyToPlay;
		SetSystemText ("Game Started!");
		StartGame ();
		Invoke ("ClearSystemText", 2.5f);
	}

    private void ClearSystemText()
    {
        SetSystemText("");
    }

    void StartQueries()
	{
		if (gameState == eGameState.ScanningCompleted)
		{
			if (spawns_ceiling == null) {
				spawns_ceiling = SpatialQueryManager.Instance.Query_FindCeilingSpawns ();
				spawns_ceiling.OnQueryFinished += SpawnPortalQueriesDone;
			}

			if (spawns_floor == null) {
				spawns_floor = SpatialQueryManager.Instance.Query_FindFloorSpawns ();
				spawns_floor.OnQueryFinished += SpawnPortalQueriesDone;
			}

			if (spawns_walls == null) {
				spawns_walls = SpatialQueryManager.Instance.Query_FindWallSpawns ();
				spawns_walls.OnQueryFinished += SpawnPortalQueriesDone;
			}
		}
	}

	[ContextMenu("Rescan Queries")]
	void ResetQueries() 
	{
		if (spawns_floor != null) 
		{
			spawns_floor.StartScan (true, false);
		}

		if (spawns_ceiling != null) 
		{
			spawns_ceiling.StartScan (true, false);
		}

		if (spawns_walls != null) 
		{
			spawns_walls.StartScan (true, false);
		}
	}

	void OnDrawGizmos() {

		float DimSize = queryhalfDim *2f;

		if (spawns_floor != null)
		{
			DrawResults (Color.blue, spawns_floor, new Vector3(DimSize, DimSize, DimSize));
		}

		if (spawns_ceiling != null)
		{
			DrawResults (Color.yellow, spawns_ceiling, new Vector3(DimSize, DimSize, DimSize));
		}

		if (spawns_walls != null)
		{
			DrawResults (Color.red, spawns_walls, new Vector3(DimSize, DimSize, DimSize));
		}
	}

	void DrawResults(Color color, SpatialQuery query, Vector3 size)
	{
		if (!query.HasResults)
			return;

		Gizmos.color = color;
		for (int i = 0; i < query.PlacementResults.Count; ++i) 
		{
			Gizmos.DrawWireCube (query.PlacementResults [i].Result.Position, size);	
		}
	}

    public void SetSystemText(string text)
    {
        if (SystemText != null)
        {
            SystemText.gameObject.SetActive(text != "");
            SystemText.text = text;
        }
    }

	private SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult GetAvailablePosition (SpatialQuery spatialQuery, SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult ignore = null)
	{
		//SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult availableResult; 
		for (int i = 0; i < spatialQuery.PlacementResults.Count; ++i) {
            if (ignore != null && spatialQuery.PlacementResults[i].Result == ignore)
                continue;

			if (Physics.CheckSphere (spatialQuery.PlacementResults [i].Result.Position, queryhalfDim * 1.5f, obstacleMask, QueryTriggerInteraction.Collide) == false) 
			{
				return spatialQuery.PlacementResults [i].Result;
			}

		}
		return null;
	}

	void SpawnFeatureAtRandomPosition(int featureIndex, int amount)
	{
		int random;
		SpatialQuery spawnSurface;

		for (int i = 0; i < amount; i++) {
			random = UnityEngine.Random.Range (0, 3);

			SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult featureResult;
			Quaternion featureRotation;
            Vector3 featurePosition;

			switch (random) {
			case 0:
				spawnSurface = spawns_ceiling;
				featureResult = GetAvailablePosition (spawnSurface);
				featureRotation = Quaternion.LookRotation (featureResult.Forward, -featureResult.Up);
                featurePosition = featureResult.Position - (-featureResult.Up * queryhalfDim);
                break;
			case 1:
				spawnSurface = spawns_floor;
				featureResult = GetAvailablePosition (spawnSurface);
				featureRotation = Quaternion.LookRotation (featureResult.Forward, featureResult.Up);
                featurePosition = featureResult.Position - (featureResult.Up * queryhalfDim);
                break;
			case 2:
				spawnSurface = spawns_walls;
				featureResult = GetAvailablePosition (spawnSurface);
				featureRotation = Quaternion.LookRotation (featureResult.Up, featureResult.Forward);
                featurePosition = featureResult.Position - (featureResult.Forward * queryhalfDim);
                break;
			default:
				spawnSurface = spawns_walls;
				featureResult = GetAvailablePosition (spawnSurface);
				featureRotation = Quaternion.LookRotation (featureResult.Up, featureResult.Forward);
                featurePosition = featureResult.Position - (featureResult.Forward * queryhalfDim);
                break;
			}

			featureSpawner.SpawnFeature (featureIndex, featurePosition, featureRotation);
		}
	}
}
