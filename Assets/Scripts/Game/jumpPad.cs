﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumpPad : MonoBehaviour {

    public Animator anim;
	float forceAmount = 500f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") {
			Rigidbody rigidbody = other.GetComponent<Rigidbody> ();
			rigidbody.velocity = new Vector3(rigidbody.velocity.x, 0, rigidbody.velocity.z);

			rigidbody.AddForce(rigidbody.transform.up * forceAmount);
           	Debug.Log("Collided with jumpPad");

            anim.SetTrigger("Trigger");
        }
    }

}
