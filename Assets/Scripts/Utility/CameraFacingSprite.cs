﻿using UnityEngine;
using System.Collections;

public class CameraFacingSprite : MonoBehaviour {
	private Transform mainCameraTransform;
	private Transform MyTransform;

	// Use this for initialization
	void Awake () {
		MyTransform = this.transform;
		mainCameraTransform = Camera.main.transform;
		//MyTransform.forward = mainCameraTransform.forward;
	}

	void OnEnable() {
		MyTransform.forward = mainCameraTransform.forward;
	}

	// Update is called once per frame
	void Update () {
		MyTransform.forward = mainCameraTransform.forward;
	}
}
