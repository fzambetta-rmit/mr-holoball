﻿using UnityEngine;
using TMPro;
using System.Collections;

public class TMP_Tween {

	public static LTDescr alpha(TMP_Text text, float to, float time)
	{
		if (time == 0.0f)
			text.alpha = to;

		float current = text.alpha;
		if (text != null)
			return LeanTween.value (text.gameObject, (System.Action<float,object>)UpdateAlpha, current, to, time).setOnUpdateParam(text);
		return null;
	}

	static void UpdateAlpha(float alpha, object obj)
	{
		TMPro.TMP_Text text = obj as TMP_Text;
		if (text != null) {
			text.alpha = alpha;
		}
	}

	public static LTDescr color(TMP_Text text, Color to, float time)
	{
		if (time == 0.0f)
			text.color = to;

		Color current = text.color;
		if (text != null)
			return LeanTween.value (text.gameObject, new Vector3 (current.r, current.g, current.b), new Vector3 (to.r, to.g, to.b), time).setOnUpdate ((System.Action<Vector3, object>)UpdateColor, text);
		return null;
	}

	static void UpdateColor(Vector3 color, object obj)
	{
		TMPro.TMP_Text text = obj as TMP_Text;
		if (text != null) {
			text.color = new Color(color.x, color.y, color.z, text.color.a);
		}
	}
}
