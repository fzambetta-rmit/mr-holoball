﻿using UnityEngine;
using RMIT.GameTech;
//using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TransformDatabase : Singleton<TransformDatabase> {

	public class TransformList {
		public Dictionary<Transform, object> transforms;

		public TransformList() 
		{
			transforms = new Dictionary<Transform, object>();
		}

		public void Add(Transform trans, object objectRef = null)
		{
			if (transforms.ContainsKey (trans) == false)
				transforms.Add (trans, objectRef);
		}

		public bool Remove(Transform trans, object objectRef = null)
		{
			return transforms.Remove(trans);
		}

		public Transform[] ToTransformArray()
		{
			Transform[] array = new Transform[transforms.Count];
			transforms.Keys.CopyTo (array, 0);
			return array;
		}

		public object[] ToObjectArray()
		{
			object[] array = new object[transforms.Count];
			transforms.Values.CopyTo (array, 0);
			return array;
		}

		public object GetRandomObject()
		{
			if (transforms.Count > 0)
				return transforms.ElementAt (Random.Range (0, transforms.Count)).Value;
			else
				return null;
		}

		public Transform GetRandomTransform()
		{
			if (transforms.Count > 0)
				return transforms.ElementAt (Random.Range (0, transforms.Count)).Key;
			else
				return null;
		}
	}
		
	private Dictionary <System.Type, TransformList> transformDatabase;

	protected TransformDatabase() {
		transformDatabase = new Dictionary<System.Type, TransformList> ();
	}
	#region Internal
	void RemoveObj(object obj, Transform trans)
	{
		if (Instance == null) 
			return;

		System.Type type = obj.GetType ();
		TransformList list = null;
		if (Instance.transformDatabase.TryGetValue (type, out list))
		{
			list.Remove (trans, obj);
		}
	}

	void AddObj(object objRef, Transform trans)
	{
		if (Instance == null)
			return;

		TransformList list = null;
		System.Type type = objRef.GetType ();
		if (Instance.transformDatabase.TryGetValue (type, out list))
		{
			list.Add (trans, objRef);
		} 
		else
		{
			list = new TransformList ();
			list.Add (trans, objRef);
			Instance.transformDatabase.Add (type, list);
		}
	}
		
	#endregion

	#region Public
	public static void Remove(object obj, Transform trans)
	{
		if (Instance != null)
			Instance.RemoveObj (obj, trans);
	}

	public static void Remove(Component obj)
	{
		if (Instance != null)
			Instance.RemoveObj(obj, obj.transform);
	}

	public static void Add(object obj, Transform trans)
	{
		if (Instance != null)
			Instance.AddObj (obj, trans);
	}

	public static void Add(Component obj)
	{
		if (Instance != null)
			Instance.AddObj (obj, obj.transform);
	}

	public object[] GetArray(System.Type type)
	{
		if (Instance == null)
			return null;

		TransformList list = null;

		if (Instance.transformDatabase.TryGetValue (type, out list)) 
		{
			return list.ToObjectArray ();
		}

		return null;
	}

	public object[] GetObjectArray(object obj)
	{
		return GetArray (obj.GetType ());
	}

	public Transform[] GetTransformArray(System.Type type)
	{
		if (Instance == null)
			return null;

		TransformList list = null;

		if (Instance.transformDatabase.TryGetValue (type, out list)) 
		{
			return list.ToTransformArray ();
		}

		return null;
	}

	public object GetRandom(System.Type type)
	{
		if (Instance == null)
			return null;

		TransformList list = null;

		if (Instance.transformDatabase.TryGetValue (type, out list)) {
			return list.GetRandomObject ();
		}
		return null;
	}

	public Transform GetRandomTransform(System.Type type)
	{
		if (Instance == null)
			return null;

		TransformList list = null;

		if (Instance.transformDatabase.TryGetValue (type, out list)) {
			return list.GetRandomTransform ();
		}
		return null;
	}

	public Transform GetClosestTransform(System.Type type, Vector3 position)
	{
		if (Instance == null) 
			return null;

		TransformList list = null;
		Transform closestTransform = null;
		float closest = Mathf.Infinity;
		if (Instance.transformDatabase.TryGetValue (type, out list))
		{
			foreach (var obj in list.transforms) {
				if (obj.Key != null) {
					float distance = (obj.Key.position - position).sqrMagnitude;
					if (distance < closest) {
						closestTransform = obj.Key;
						closest = distance;
					}
				}
			}
		}
			
		return closestTransform;
	}

	public object GetClosest(System.Type type, Vector3 position)
	{
		if (Instance == null) 
			return null;

		TransformList list = null;
		object closestObj = null;
		float closest = Mathf.Infinity;
		if (Instance.transformDatabase.TryGetValue (type, out list))
		{
			foreach (var obj in list.transforms) {
				if (obj.Value != null && obj.Key != null) {
					float distance = (obj.Key.position - position).sqrMagnitude;
					if (distance < closest) {
						closestObj = obj.Value;
						closest = distance;
					}
				}
			}
		}

		return closestObj;
	}

	#endregion
}
