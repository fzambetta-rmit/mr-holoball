﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using HoloToolkit.Unity;

namespace SpatialQueries
{
	public class SpatialQuery {

		// Enums
		public enum QueryStates
		{
			None,
			WaitingForScanCompletion,
			Processing,
			Finished
		}
			

		// Structs
		private struct QueryStatus
		{
			public void Reset()
			{
				State = QueryStates.None;
				//Name = "";
				CountFail = 0;
				CountSuccess = 0;
				QueryResult = new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult>();
			}

			public QueryStates State;
			//public string Name;
			public int CountFail;
			public int CountSuccess;
			public List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult> QueryResult;

		}

		private QueryStatus queryStatus;
		public delegate void OnQueryFinishedDelegate();
		public event OnQueryFinishedDelegate OnQueryFinished;
		public string Name;
		public List<PlacementResult> PlacementResults = new List<PlacementResult>();
		private List<PlacementQuery> placementQuery = new List<PlacementQuery> ();
		private bool removeObjects = false;
		private bool _invokeCallback = false;

		public bool HasResults {
			get {
				return (queryStatus.State != QueryStates.Processing && PlacementResults.Count > 0);
			}
		}

		public SpatialQuery(string name, List<PlacementQuery> placementList, bool clearAllObjects = false, bool invokeCallback = true)
		{
			queryStatus = new QueryStatus ();
			Name = name;
			placementQuery = placementList;
			StartScan (clearAllObjects, invokeCallback);

		}
		
		// Update is called once per frame
		void Update () {
			// Can't do any of this till we're done with the scanning phase
			if (SpatialUnderstanding.Instance.ScanState != SpatialUnderstanding.ScanStates.Done)
			{
				return;
			}

			ProcessPlacementResults ();
		}

		public bool UpdateQueryList(List<PlacementQuery> query, bool removeObj = false)
		{
			if (queryStatus.State == QueryStates.Processing || queryStatus.State == QueryStates.Finished)
				return false;

			placementQuery.Clear ();
			placementQuery = query;
			removeObjects = removeObj;
			return true;
		}

		public bool StartScan(bool removeObj = false, bool invokeCallback = true)
		{
			if (queryStatus.State == QueryStates.Processing || queryStatus.State == QueryStates.Finished)
				return false;

			if (SpatialUnderstanding.Instance.ScanState != SpatialUnderstanding.ScanStates.Done) {
				queryStatus.State = QueryStates.WaitingForScanCompletion;
				return false;
			}

			_invokeCallback = invokeCallback;
			removeObjects = removeObj;
			PlaceObjectAsync (placementQuery, removeObjects);
			return true;
		}

		private bool PlaceObjectAsync(
			//string placementName,
			List<PlacementQuery> placementList,
			bool clearObjectsFirst = true)
		{
			// If we already mid-query, reject the request
			if (queryStatus.State != QueryStates.None)
			{
				return false;
			}

			// Clear geo
			if (clearObjectsFirst)
			{
				RemoveObjects();
			}

			// Mark it
			queryStatus.Reset();
			queryStatus.State = QueryStates.Processing;
			//Name = placementName;

			// Kick off a thread to do process the queries
			#if UNITY_EDITOR || !UNITY_WSA
			new System.Threading.Thread
			#else
			System.Threading.Tasks.Task.Run
			#endif
			(() =>
				{
					// Go through the queries in the list
					for (int i = 0; i < placementList.Count; ++i)
					{
						// Do the query
						bool success = PlaceObject(
							//placementName,
							placementList[i].PlacementDefinition,
							placementList[i].PlacementRules,
							placementList[i].PlacementConstraints, 
							clearObjectsFirst, 
							true);

						// Mark the result
						queryStatus.CountSuccess = success ? (queryStatus.CountSuccess + 1) : queryStatus.CountSuccess;
						queryStatus.CountFail = !success ? (queryStatus.CountFail + 1) : queryStatus.CountFail;
					}

					// Done
					queryStatus.State = QueryStates.Finished;
				}
			)
			#if UNITY_EDITOR || !UNITY_WSA
				.Start()
			#endif
				;

			return true;
		}

		private bool PlaceObject(
			//string placementName,
			SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition placementDefinition,
			List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule> placementRules = null,
			List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint> placementConstraints = null,
			bool clearObjectsFirst = true,
			bool isASync = false)
		{
			// Clear objects (if requested)
			if (!isASync && clearObjectsFirst)
			{
				RemoveObjects();
			}
			if (!SpatialUnderstanding.Instance.AllowSpatialUnderstanding)
			{
				return false;
			}

			// New query
			if (SpatialUnderstandingDllObjectPlacement.Solver_PlaceObject(
				Name,
				SpatialUnderstanding.Instance.UnderstandingDLL.PinObject(placementDefinition),
				(placementRules != null) ? placementRules.Count : 0,
				((placementRules != null) && (placementRules.Count > 0)) ? SpatialUnderstanding.Instance.UnderstandingDLL.PinObject(placementRules.ToArray()) : IntPtr.Zero,
				(placementConstraints != null) ? placementConstraints.Count : 0,
				((placementConstraints != null) && (placementConstraints.Count > 0)) ? SpatialUnderstanding.Instance.UnderstandingDLL.PinObject(placementConstraints.ToArray()) : IntPtr.Zero,
				SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticObjectPlacementResultPtr()) > 0)
			{
				SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult placementResult = SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticObjectPlacementResult();
				if (!isASync)
				{
					// If not running async, we can just add the results to the draw list right now
					PlacementResults.Add(new PlacementResult(placementResult.Clone() as SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult));
				}
				else
				{
					queryStatus.QueryResult.Add(placementResult.Clone() as SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult);
				}
				return true;
			}
			if (!isASync)
			{
				Debug.Log(Name + " (0)");
			}
			return false;
		}

		public void ProcessPlacementResults()
		{
			if (!SpatialUnderstanding.Instance.AllowSpatialUnderstanding)
			{
				return;
			}

			// Check it
			if (queryStatus.State == QueryStates.WaitingForScanCompletion)
			{
				// Trigger query once scanning is completed.
				StartScan(removeObjects, _invokeCallback);
			}
			else if (queryStatus.State != QueryStates.Finished)
			{
				return;
			}
			
			// Clear results
			// RemoveObjects();
			PlacementResults.Clear();

			// We will reject any above or below the ceiling/floor
			SpatialUnderstandingDll.Imports.QueryPlayspaceAlignment(SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignmentPtr());
			SpatialUnderstandingDll.Imports.PlayspaceAlignment alignment = SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignment();

			// Copy over the results
			for (int i = 0; i < queryStatus.QueryResult.Count; ++i)
			{
				if ((queryStatus.QueryResult[i].Position.y < alignment.CeilingYValue) &&
					(queryStatus.QueryResult[i].Position.y > alignment.FloorYValue))
				{
					PlacementResults.Add(new PlacementResult(queryStatus.QueryResult[i].Clone() as SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult));
				}
			}

			// Text
			Debug.Log(Name + " (" + PlacementResults.Count + "/" + (queryStatus.CountSuccess + queryStatus.CountFail) + ")");

			if (OnQueryFinished != null && _invokeCallback)
				OnQueryFinished.Invoke ();
			
			// Mark done
			queryStatus.Reset();
		}

		public void RemoveObjects()
		{
			SpatialUnderstandingDllObjectPlacement.Solver_RemoveObject (Name);
		}
	}
}