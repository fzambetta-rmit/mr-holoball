﻿using UnityEngine;
using System.Collections;
using HoloToolkit.Unity;
using System.Collections.Generic;
using System;
using SpatialQueries;

namespace SpatialQueries
{

	public struct PlacementQuery
	{
		public PlacementQuery(
			SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition placementDefinition,
			List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule> placementRules = null,
			List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint> placementConstraints = null)
		{
			PlacementDefinition = placementDefinition;
			PlacementRules = placementRules;
			PlacementConstraints = placementConstraints;
		}

		public SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition PlacementDefinition;
		public List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule> PlacementRules;
		public List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint> PlacementConstraints;
	}

	public class PlacementResult
	{
		public PlacementResult(SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult result)
		{
			Result = result;
		}

		public SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult Result;
	}
}

public class SpatialQueryManager : MonoBehaviour
{
    // Singleton
    public static SpatialQueryManager Instance;

    // Properties
    public bool IsSolverInitialized { get; private set; }

    // Privates
	private Dictionary<string, SpatialQuery> spatialQueries = new Dictionary<string, SpatialQuery> ();

    // Functions
    private void Awake()
    {
        Instance = this;
    }

	public void ClearAllGeometry ()
	{
		//placementResults.Clear();
		if (SpatialUnderstanding.Instance.AllowSpatialUnderstanding)
		{
			SpatialUnderstandingDllObjectPlacement.Solver_RemoveAllObjects();
		}
	}

	public void ClearGeometry(string placementName)
    {
		if (SpatialUnderstanding.Instance.AllowSpatialUnderstanding)
		{
			SpatialUnderstandingDllObjectPlacement.Solver_RemoveObject (placementName); 
		}
    }
			
	#if FALSE // SAMPLE
    public void Query_OnFloor()
    {
        List<PlacementQuery> placementQuery = new List<PlacementQuery>();
        for (int i = 0; i < 4; ++i)
        {
            float halfDimSize = UnityEngine.Random.Range(0.15f, 0.35f);
            placementQuery.Add(
                new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnFloor(new Vector3(halfDimSize, halfDimSize, halfDimSize * 2.0f)),
                                    new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 3.0f),
                                    }));
        }
        PlaceObjectAsync("OnFloor", placementQuery);
    }

    public void Query_OnWall()
    {
        List<PlacementQuery> placementQuery = new List<PlacementQuery>();
        for (int i = 0; i < 6; ++i)
        {
            float halfDimSize = UnityEngine.Random.Range(0.3f, 0.6f);
            placementQuery.Add(
                new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnWall(new Vector3(halfDimSize, halfDimSize * 0.5f, 0.05f), 0.5f, 3.0f),
                                    new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 4.0f),
                                    }));
        }
        PlaceObjectAsync("OnWall", placementQuery);
    }

    public void Query_OnCeiling()
    {
        List<PlacementQuery> placementQuery = new List<PlacementQuery>();
        for (int i = 0; i < 2; ++i)
        {
            float halfDimSize = UnityEngine.Random.Range(0.3f, 0.4f);
            placementQuery.Add(
                new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnCeiling(new Vector3(halfDimSize, halfDimSize, halfDimSize)),
                                    new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 3.0f),
                                    }));
        }
        PlaceObjectAsync("OnCeiling", placementQuery);
    }

    public void Query_OnEdge()
    {
        List<PlacementQuery> placementQuery = new List<PlacementQuery>();
        for (int i = 0; i < 8; ++i)
        {
            float halfDimSize = UnityEngine.Random.Range(0.05f, 0.1f);
            placementQuery.Add(
                new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnEdge(new Vector3(halfDimSize, halfDimSize, halfDimSize),
                                                                                                                    new Vector3(halfDimSize, halfDimSize, halfDimSize)),
                                    new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 3.0f),
                                    }));
        }
        PlaceObjectAsync("OnEdge", placementQuery);
    }

    public void Query_OnFloorAndCeiling()
    {
        SpatialUnderstandingDll.Imports.QueryPlayspaceAlignment(SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignmentPtr());
        SpatialUnderstandingDll.Imports.PlayspaceAlignment alignment = SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignment();
        List<PlacementQuery> placementQuery = new List<PlacementQuery>();
        for (int i = 0; i < 4; ++i)
        {
            float halfDimSize = UnityEngine.Random.Range(0.1f, 0.2f);
            placementQuery.Add(
                new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnFloorAndCeiling(new Vector3(halfDimSize, (alignment.CeilingYValue - alignment.FloorYValue) * 0.5f, halfDimSize),
                                                                                                                            new Vector3(halfDimSize, (alignment.CeilingYValue - alignment.FloorYValue) * 0.5f, halfDimSize)),
                                    new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 3.0f),
                                    }));
        }
        PlaceObjectAsync("OnFloorAndCeiling", placementQuery);
    }

    public void Query_RandomInAir_AwayFromMe()
    {
        List<PlacementQuery> placementQuery = new List<PlacementQuery>();
        for (int i = 0; i < 8; ++i)
        {
            float halfDimSize = UnityEngine.Random.Range(0.1f, 0.2f);
            placementQuery.Add(
                new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_RandomInAir(new Vector3(halfDimSize, halfDimSize, halfDimSize)),
                                    new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 3.0f),
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromPosition(Camera.main.transform.position, 2.5f),
                                    }));
        }
        PlaceObjectAsync("RandomInAir - AwayFromMe", placementQuery);
    }

    public void Query_OnEdge_NearCenter()
    {
        List<PlacementQuery> placementQuery = new List<PlacementQuery>();
        for (int i = 0; i < 4; ++i)
        {
            float halfDimSize = UnityEngine.Random.Range(0.05f, 0.1f);
            placementQuery.Add(
                new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnEdge(new Vector3(halfDimSize, halfDimSize, halfDimSize), new Vector3(halfDimSize, halfDimSize, halfDimSize)),
                                    new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 2.0f),
                                    },
                                    new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint>() {
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint.Create_NearCenter(),
                                    }));
        }
        PlaceObjectAsync("OnEdge - NearCenter", placementQuery);

    }

    public void Query_OnFloor_AwayFromMe()
    {
        List<PlacementQuery> placementQuery = new List<PlacementQuery>();
        for (int i = 0; i < 4; ++i)
        {
            float halfDimSize = UnityEngine.Random.Range(0.05f, 0.15f);
            placementQuery.Add(
                new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnFloor(new Vector3(halfDimSize, halfDimSize, halfDimSize)),
                                    new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromPosition(Camera.main.transform.position, 2.0f),
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 3.0f),
                                    }));
        }
        PlaceObjectAsync("OnFloor - AwayFromMe", placementQuery);
    }

    public void Query_OnFloor_NearMe()
    {
        List<PlacementQuery> placementQuery = new List<PlacementQuery>();
        for (int i = 0; i < 4; ++i)
        {
            float halfDimSize = UnityEngine.Random.Range(0.05f, 0.2f);
            placementQuery.Add(
                new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnFloor(new Vector3(halfDimSize, halfDimSize, halfDimSize)),
                                    new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 3.0f),
                                    },
                                    new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint>() {
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint.Create_NearPoint(Camera.main.transform.position, 0.5f, 2.0f)
                                    }));
        }
        PlaceObjectAsync("OnFloor - NearMe", placementQuery);
    }

    public void Query_Portals()
    {
        if (SpatialUnderstanding.Instance.ScanState != SpatialUnderstanding.ScanStates.Done)
            return;

        SpatialUnderstandingDll.Imports.QueryPlayspaceAlignment(SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignmentPtr());
        SpatialUnderstandingDll.Imports.PlayspaceAlignment alignment = SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignment();
        List<PlacementQuery> placementQuery = new List<PlacementQuery>();
        for (int i = 0; i < 4; ++i)
        {
            float halfDimSize = UnityEngine.Random.Range(0.1f, 0.2f);
            placementQuery.Add(
                new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnFloorAndCeiling(new Vector3(halfDimSize, (alignment.CeilingYValue - alignment.FloorYValue) * 0.5f, halfDimSize),
                                                                                                                            new Vector3(halfDimSize, (alignment.CeilingYValue - alignment.FloorYValue) * 0.5f, halfDimSize)),
                                    new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
                                        SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 3.0f),
                                    }));
        }
        PlaceObjectAsync("Portals", placementQuery);
    }
	#endif

	/*public void Query_Test() 
	{
		List<PlacementQuery> placementQuery = new List<PlacementQuery>();
		for (int i = 0; i < 8; ++i)
		{
			float halfDimSize = UnityEngine.Random.Range(0.015f, 0.02f);
			placementQuery.Add(
				new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_RandomInAir(new Vector3(halfDimSize, halfDimSize, halfDimSize)),
					new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
						SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 3.0f),
						SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromPosition(Camera.main.transform.position, 2.5f),
					}));
		}

		PlaceObjectAsync("Query_Test", placementQuery);
	}*/

	public SpatialQuery CreateQuery(string name, List<PlacementQuery> placementQuery, bool removeObjects = false)
	{
		SpatialQuery newQuery;
		spatialQueries.TryGetValue (name, out newQuery);
		if (newQuery == null) {
			newQuery = new SpatialQuery (name, placementQuery, removeObjects);
			spatialQueries.Add (name, newQuery);
		} else {
			newQuery.UpdateQueryList (placementQuery, removeObjects);
		}
		return newQuery;
	}

	const float halfDimSize = 0.08f;

	public SpatialQuery Query_FindFloorSpawns()
	{
		SpatialUnderstandingDll.Imports.QueryPlayspaceAlignment(SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignmentPtr());
		List<PlacementQuery> placementQuery = new List<PlacementQuery>();

		// Find on floor
		for (int i = 0; i < 10; ++i)
		{
			//float halfDimSize = 0.03f;//UnityEngine.Random.Range(0.01f, 0.015f);
			placementQuery.Add(
				new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnFloor(new Vector3(halfDimSize, halfDimSize, halfDimSize)),
					new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
						SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 5.0f),
					}));
		}

		return CreateQuery ("Spawns_Floor", placementQuery);
	}

	public SpatialQuery Query_FindWallSpawns()
	{
		SpatialUnderstandingDll.Imports.QueryPlayspaceAlignment(SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignmentPtr());

		List<PlacementQuery> placementQuery = new List<PlacementQuery>();
		// Find 2 on walls.
		for (int i = 0; i < 10; ++i)
		{
			//float halfDimSize = 0.03f;//UnityEngine.Random.Range(0.01f, 0.015f);
			placementQuery.Add(
				new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnWall(new Vector3(halfDimSize, halfDimSize, halfDimSize), 0.5f, 3.0f),
					new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
						SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 5.0f),
					}));
		}

		return CreateQuery ("Spawns_Wall", placementQuery);
	}

	public SpatialQuery Query_FindCeilingSpawns()
	{
		SpatialUnderstandingDll.Imports.QueryPlayspaceAlignment(SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignmentPtr());

		List<PlacementQuery> placementQuery = new List<PlacementQuery>();

		// find 2 on ceiling
		for (int i = 0; i < 10; ++i)
		{
			//float halfDimSize = 0.03f;//UnityEngine.Random.Range(0.01f, 0.015f);
			placementQuery.Add(
				new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnCeiling(new Vector3(halfDimSize , halfDimSize, halfDimSize)),
					new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
						SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 5.0f),
					}));
		}

		return CreateQuery ("Spawn_Ceiling", placementQuery);
	}

	public SpatialQuery Query_SpawnAndEndPortals()
	{
		SpatialUnderstandingDll.Imports.QueryPlayspaceAlignment(SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignmentPtr());

		//SpatialUnderstandingDll.Imports.PlayspaceAlignment alignment = SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignment();
		List<PlacementQuery> placementQuery = new List<PlacementQuery>();
		// Find 2 on floor
		for (int i = 0; i < 3; ++i)
		{
			float halfDimSize = 0.015f;//UnityEngine.Random.Range(0.01f, 0.015f);
			placementQuery.Add(
				new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnFloor(new Vector3(halfDimSize, halfDimSize, 0f)),
					new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
						SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 4.0f),
					}));
		}

		// Find 2 on walls.
		for (int i = 0; i < 3; ++i)
		{
			float halfDimSize = UnityEngine.Random.Range(0.01f, 0.015f);
			placementQuery.Add(
				new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnWall(new Vector3(halfDimSize, halfDimSize, halfDimSize), 0.5f, 3.0f),
					new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
						SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 4.0f),
					}));
		}

		// find 2 on ceiling
		for (int i = 0; i < 3; ++i)
		{
			float halfDimSize = UnityEngine.Random.Range(0.010f, 0.015f);
			placementQuery.Add(
				new PlacementQuery(SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnCeiling(new Vector3(halfDimSize, halfDimSize, halfDimSize)),
					new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
						SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(halfDimSize * 3.0f),
					}));
		}

		return CreateQuery ("SpawnAndEndPortals", placementQuery);
	}

    public bool InitializeSolver()
    {
        if (IsSolverInitialized ||
            !SpatialUnderstanding.Instance.AllowSpatialUnderstanding)
        {
            return IsSolverInitialized;
        }

        if (SpatialUnderstandingDllObjectPlacement.Solver_Init() == 1)
        {
            IsSolverInitialized = true;
        }
        return IsSolverInitialized;
    }

    private void Update()
    {
        // Can't do any of this till we're done with the scanning phase
        if (SpatialUnderstanding.Instance.ScanState != SpatialUnderstanding.ScanStates.Done)
        {
            return;
        }

        // Make sure the solver has been initialized
        if (!IsSolverInitialized &&
            SpatialUnderstanding.Instance.AllowSpatialUnderstanding)
        {
            InitializeSolver();
        }

		foreach(SpatialQuery query in spatialQueries.Values)
		{
			query.ProcessPlacementResults ();
		}
    }
}