﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RMIT.GameTech;

public class ComponentDatabase : Singleton<ComponentDatabase> {

	public class ComponentList {
		private LinkedList<Component> components;

		public ComponentList() 
		{
			components = new LinkedList<Component>();
		}

		public void Add(Component comp)
		{
			components.AddLast (comp);
		}

		public void Remove(Component comp)
		{
			components.Remove(comp);
		}
	
		public Component[] ToArray()
		{
			Component[] array = new Component[components.Count];
			components.CopyTo(array, 0);
			return array;
		}
	}

	//private Dictionary <string, ComponentList> tagsDatabase;
	private Dictionary <System.Type, ComponentList> database;

	protected ComponentDatabase() {
		database = new Dictionary<System.Type, ComponentList> ();
	}

	// Use this for initialization
	/*void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}*/
	public static void Remove(Component comp)
	{
		if (Instance != null)
			Instance.RemoveComp (comp);
	}

	public static void Add(Component comp)
	{
		if (Instance != null)
			Instance.AddComp (comp);
	}

	void RemoveComp(Component comp)
	{
		if (Instance == null) 
			return;
	
		System.Type type = comp.GetType ();
		ComponentList list = null;
		if (Instance.database.TryGetValue (type, out list))
		{
			list.Remove (comp);
		}
	}

	void AddComp (Component comp)
	{
		if (Instance == null)
			return;

		ComponentList list = null;
		System.Type type = comp.GetType ();
		if (Instance.database.TryGetValue (type, out list))
		{
			list.Add (comp);
		} 
		else
		{
			list = new ComponentList ();
			list.Add (comp);
			Instance.database.Add (type, list);
		}
	}

	public Component[] GetArray(System.Type type)
	{
		if (Instance == null)
			return null;

		ComponentList list = null;

		if (Instance.database.TryGetValue (type, out list)) 
		{
			return list.ToArray ();
		}

		return null;
	}

	public Component[] GetArray(Component comp)
	{
		return GetArray (comp.GetType ());
	}

	public Component GetClosest(System.Type type, Vector3 position)
	{
		Component[] array = GetArray (type);

		if (array != null) {
			Component closestComp = null;
			float closest = Mathf.Infinity;
			foreach (var comp in array) {
				if (comp != null) {
					float distance = (comp.transform.position - position).sqrMagnitude;
					if (distance < closest) {
						closestComp = comp;
						closest = distance;
					}
				} else {
					Debug.Log ("TODO: Remove missing componenet!");	
				}
			}
			return closestComp;
		}
		return null;
	}
}
